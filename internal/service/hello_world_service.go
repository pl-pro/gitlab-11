package service

import (
	"context"

	. "gitlab.com/pl-pro/gitlab-11/api"
)

// Service type
//  Helloworld Service

type HelloWorldService struct {
	UnimplementedHelloWorldServiceServer
}

// Create a new service
func NewHelloWorldService() *HelloWorldService {
	service := &HelloWorldService{}
	return service
}

//  Sends a greeting
func (service *HelloWorldService) SayHello(ctx context.Context, request *SayHelloRequest) (*SayHelloResponse, error) {
	return &SayHelloResponse{}, nil
}
